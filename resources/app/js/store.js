// import { createApp } from 'vue';
import { createStore } from 'vuex'
import script from './script'

const store = createStore({  

    state : { 

        token: localStorage.getItem('token') || '',
        token_ttl: localStorage.getItem('token_ttl')  || '',
        user: {},
        userLoaded: false,
        keep_login: false,
        loginProcess: false,
        userName: ''

    },
    mutations : { 

        login(state, obj) {
            // const now = new Date()

            localStorage.setItem("token", obj.token)
            localStorage.setItem("token_ttl", obj.token_ttl)
            localStorage.setItem("keep_login", obj.keep_login)
        },
        logout(state) {
            localStorage.removeItem("token")
            localStorage.removeItem("token_ttl")
        },
        setUser(state, obj){
            
            state.user = obj
        },
        setUserLoaded(state, bool){
            
            state.userLoaded = bool
        },
        setLoginProcess(state,bool) {

            state.loginProcess = bool
        },
        setUserName(state,str) {
            state.userName = str
        }

    },
    actions: {  
        async getUserInfo(context){ 

            const api_uri = import.meta.env.VITE_API_URL

            try {
                
                let res = await axios.get(`${api_uri}/auth/users`);

                context.commit('setUserLoaded', true)
                context.commit('setLoginProcess',true)
                context.commit('setUser', res.data.data.user)
                context.commit('setUserName',res.data.data.user.name)

                return res;

            } catch (error) {
                script.goToLogout()
            }
        }
    },
    getters: {
        getUserLoaded (state) {
          return state.userLoaded
        },
        getLoginProcess (state) {
            return state.loginProcess
        }
    }
})

export default store
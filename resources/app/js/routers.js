import { createRouter } from 'vue-router'
import loggedinPage from '../pages/loggedin.vue'
import dashboardPage from '../pages/dashboard.vue'
import employeePage from '../pages/employee.vue'
import companyPage from '../pages/company.vue'
import addCompany from '../pages/addCompany.vue'
import addEmployee from '../pages/addEmployee.vue'
import editCompany from '../pages/editCompany.vue'
import EditEmployee from '../pages/editEmployee.vue'

const siteName = import.meta.env.VITE_APP_NAME

const routes = [

    {
      path: '/loggedin',
      component: loggedinPage,
      name: 'Loggedin',
      meta: {
            
        title : `Loggedin | ${siteName}`
      },
    },
    {
      path: '/',
      component: dashboardPage,
      name: 'Dashboard',
      alias: '/dashboard',
      meta: {
            
        title : `Dashboard | ${siteName}`
      }
    },
    {
      path: '/company',
      component: companyPage,
      name: 'Company',
      meta: {
            
        title : `Company | ${siteName}`
      },
    },
    {
      path: '/employee',
      component: employeePage,
      name: 'Employee',
      meta: {
            
        title : `Employee | ${siteName}`
      },
    },
    {
      path: '/company/add',
      component: addCompany,
      name: 'Add Company',
      meta: {
            
        title : `Add Company | ${siteName}`
      },
    },
    {
      path: '/employee/add',
      component: addEmployee,
      name: 'Add Employee',
      meta: {
            
        title : `Add Employee | ${siteName}`
      },
    },
    {
      path: '/company/:uuid',
      component: editCompany,
      name: 'Edit Company',
      meta: {
            
        title : `Edit Company | ${siteName}`
      },
    },
    {
      path: '/employee/:uuid',
      component: EditEmployee,
      name: 'Edit Employee',
      meta: {
            
        title : `Edit Employee | ${siteName}`
      },
    },
  ]

export default function (history) {
    return createRouter({
      history,
      routes
    })
  }
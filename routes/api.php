<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;
use App\Http\Middleware\IsAdmin;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/refresh', Api\Auth\RefreshController::class);
Route::post('/logout', Api\Auth\LogoutController::class);
Route::get('/auth/users', Api\Auth\UserController::class)->middleware('auth:api');


Route::middleware(['auth:api'])->group(function () { 

    Route::resource('companies',Api\CompanyController::class)->only(['index','store','show','update','destroy'])->parameters(['' => 'uuid'])->middleware(IsAdmin::class);
    Route::resource('employees',Api\EmployeeController::class)->only(['index','store','show','update','destroy'])->parameters(['' => 'uuid']);
});
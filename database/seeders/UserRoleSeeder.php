<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'email' => 'admin@grtech.com',
                'role' => 'admin',
            ],
            [
                'email' => 'user@grtech.com',
                'role' => 'user',
            ],
           
        ];

        foreach ($data as $item){

            $user = User::where('email',$item['email'])->first();
            $user->getRoleNames();
            $roles = collect($user->roles)->pluck('name')->toArray();
            if (in_array($item['role'], $roles)) continue;

            $roleToAssign = Role::findByName($item['role']);
            $user->assignRole($roleToAssign);
        }
    }
}

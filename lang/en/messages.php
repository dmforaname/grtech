<?php

return [

   'create' => ':X Successfully Saved',
   'retrieve' => ':X retrieved successfully',
   'retrieve_error' => 'Failed to retrieved data',
   'update' => ':X Updated',
   'saving_error' => 'Saving Error',
   'delete_failed' => 'You cant delete this :X',
   'delete_check' => 'You cant delete<br>This :X using by :Y',
   'delete' => ':X Deleted',
   'error' => 'An error occurred',
   'update_check' => 'You cant update<br>This :X using by :Y',
   'success' => 'Success',
   'slug_error' => ':X has already been taken.',
];
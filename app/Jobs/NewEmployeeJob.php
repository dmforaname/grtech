<?php

namespace App\Jobs;

use App\Models\Company;
use App\Models\Employee;
use App\Notifications\NewEmployeeNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class NewEmployeeJob implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public Employee $employee
        )
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $company = Company::find($this->employee->company_id);

        $details = [

            'greeting' => 'Hi '.$company->name,
            'body' => 'You have new employee',
            'name' => 'Name: '.$this->employee->full_name,
            'email' => 'Email: ' .$this->employee->email,
            'phone' => 'Phone: ' .$this->employee->phone,
            'thanks' => 'Thank you for using grtech!',
            'data_id' => 'Id: '.$this->employee->id,
        ];

        Notification::route('mail', [
            $company->email => $company->name,
        ])->notify(new NewEmployeeNotification($details));
    }
}

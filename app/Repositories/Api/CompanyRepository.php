<?php

namespace App\Repositories\Api;

use App\Http\Resources\Api\CompanyTableResource;
use App\Models\Company;
use App\Repositories\BaseRepository;
use App\Traits\ImageTrait;
use Illuminate\Support\Facades\Log;

/**
 * Class CompanyRepository
 *
 * @package App\Repositories\Api
 */
class CompanyRepository extends BaseRepository
{
    use ImageTrait;

    protected $image;

    /**
     * CompanyRepository constructor.
     *
     * @param  Company  $model
     */
    public function __construct(public Company $model)
    {
        parent::__construct($model);
    }   

    public function getNewParam($request, $image = null)
    {
        $params = $request->validated();
        $this->image = 'logo';

        if ($request->hasFile($this->image)) {

            if ($image) $this->removeImage($image);
            
            $params[$this->image] = $this->storeImageToStorage($request,$this->image);
        }

        return $params;
    }

    public function getShowData($id)
    {
        return $this->model->query()->Uuid($id);
    }

    public function getTableData($request)
    {
        $model = $this->model->query();

        $recordsTotal =  $model->count();

        if ($request->results) {

            $model->take($request->results);

            if ($request->page) $model->skip(($request->page - 1) * $request->results);
        }

        $data = $model->get();

        if ($data) {
            return [
                'data' => CompanyTableResource::collection($data),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => count($data),
                'message' => __('messages.retrieve',['X' => 'Company'])
            ];
        }

       return false;
    }

    public function getAll()
    {
        return $this->model->get();
    }
}
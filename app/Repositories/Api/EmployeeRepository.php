<?php

namespace App\Repositories\Api;

use App\Http\Resources\Api\EmployeeTableResource;
use App\Models\Employee;
use App\Repositories\BaseRepository;

/**
 * Class CompanyRepository
 *
 * @package App\Repositories\Api
 */
class EmployeeRepository extends BaseRepository
{

    protected $image;

    /**
     * EmployeeRepository constructor.
     *
     * @param  Employee $model
     */
    public function __construct(public Employee $model)
    {
        parent::__construct($model);
    } 
    
    public function getShowData($id)
    {
        $model = $this->model->query();
        $model->where('uuid' ,$id);
        $model->with('company');
        
        return $model->first();
    }

    public function getTableData()
    {
        $model = $this->model->query();

        $recordsTotal =  $model->count();
        $model->with('company');
        
        $data = $model->get();

        if ($data) {
            return [
                'data' => EmployeeTableResource::collection($data),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => count($data),
                'message' => __('messages.retrieve',['X' => 'Employee'])
            ];
        }

       return false;
    }
}
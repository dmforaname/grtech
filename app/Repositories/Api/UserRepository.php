<?php

namespace App\Repositories\Api;

use App\Models\User;
use App\Repositories\BaseRepository;
use Spatie\Permission\Models\Role;

/**
 * Class UserRepository
 *
 * @package App\Repositories\Api
 */
class UserRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    public function __construct(public User $model)
    {
        parent::__construct($model);
    }   
}
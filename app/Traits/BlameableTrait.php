<?php

namespace App\Traits;

use App\Observers\BlamableObserver;
use Illuminate\Support\Facades\Log;

trait BlameableTrait
{
    public static function bootBlameableTrait()
    {
        static::observe(BlamableObserver::class);
    }
}

<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait ImageTrait
{
    protected $path = "images";

    /**
     * Store Image to storage.
     *
     * @param  object $request
     * @param  string $file
     * @param  string $folder
     */
    public function storeImageToStorage($request, $file, $folder = null)
    {        
        if ($folder) $this->path = $this->path."/".$folder;

        Storage::makeDirectory($this->path);

        $image = $request->file($file);
        $path = Storage::disk('public')->putFile($this->path, $image);
        $url = Storage::url($path);

        return $url;
    }

    /**
     * Remove Image from storage.
     *
     * @param string $path
     */
    public function removeImage($path) :void
    {
        Log::info("Original image path to delete: $path");
        $newPath = str_replace("storage/","",$path);
        Log::info("Deleted image path: $newPath");
        Storage::disk('public')->delete($newPath);
    }
}

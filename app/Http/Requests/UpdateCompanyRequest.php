<?php

namespace App\Http\Requests;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class UpdateCompanyRequest extends FormRequest
{
    
    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::any(['admin']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        try {

            $id = Company::Uuid($this->route('company'))->id;
        } catch (\Exception $e) {

            // abort(401, $e->getMessage());
            abort(422, 'Data not found!');
        }
       
        return [
            
            'name' => 'bail|required|string',
            'email' => 'bail|required|email:rfc,dns|unique:companies,email,'.$id,
            'logo' => 'bail|sometimes|required|image',
            'website' => 'bail|required|url:http,https,'
        ];
    }
}

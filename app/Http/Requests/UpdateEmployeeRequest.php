<?php

namespace App\Http\Requests;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;
    
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::any(['admin','user']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        try {

            $id = Employee::Uuid($this->route('employee'))->id;
        } catch (\Exception $e) {

            // abort(401, $e->getMessage());
            abort(422, 'Data not found!');
        }

        return [
            'first_name' => 'bail|required|string',
            'last_name' => 'bail|required|string',
            'phone' => 'bail|required|numeric',
            'email' => 'bail|required|email:rfc,dns|unique:employees,email,'.$id,
            'company_id' => 'bail|required|uuid|exists:companies,uuid',
        ];
    }

    /**
     * Modify validated data
     *
     * @return array
     */
    public function validated($key = null, $default = null): array
    {
        $data = parent::validated();
        $company = Company::uuid($data['company_id']);
        $data['company_id'] = $company['id'];
        
        return $data;
    }
}

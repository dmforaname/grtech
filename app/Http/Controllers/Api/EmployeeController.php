<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Http\Resources\Api\CompanyListResource;
use App\Http\Resources\Api\EmployeeShowRequest;
use App\Jobs\NewEmployeeJob;
use App\Models\Employee;
use App\Repositories\Api\CompanyRepository;
use App\Repositories\Api\EmployeeRepository;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Log;

class EmployeeController extends Controller
{
    use ApiResponser;

    /**
     * EmployeeController constructor.
     *
     * @param  EmployeeRepository $employee
     */
    public function __construct(
        private EmployeeRepository $employee,
        private CompanyRepository $company
        )
    {
        //
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        if (request()->list) {

            if (request()->list == 'company') {
                $data = $this->company->getAll();

                if ($data) 
                    return $this->success(CompanyListResource::collection($data),__('messages.retrieve',['X' => 'Company']));
            }
            
        }

        if (request()->ajax()) { 

   
            $gets = $this->employee->getTableData(request());

            return $this->responseJson($gets);
        }

        return $this->error(__('messages.error'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeRequest $request)
    {
        $data = $this->employee->create($request->validated());

        if($data) {

            NewEmployeeJob::dispatch($data);

            return $this->success($data, __('messages.retrieve',['X' => 'Employee']));
        }

        return $this->error(__('messages.error'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = $this->employee->getShowData($id);

        if ($data) return $this->success(EmployeeShowRequest::make($data),trans('messages.retrieve',['X' => 'Employee']));

        return $this->error(__('messages.retrieve_error'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEmployeeRequest $request, string $id)
    {
        $data = $this->employee->getByUuid($id);

        if ($data) {
            $data->update($request->validated());
            return $this->success($data,trans('messages.update',['X' => 'Company']));
        }

        return $this->error(__('messages.error'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = $this->employee->getByUuid($id);   

        if ($data) {

            $data->delete();

            return $this->success([],trans('messages.delete',['X' => 'Employee']));
        }

        return $this->error(__('messages.error'));
    }
}

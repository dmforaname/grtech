<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Resources\Api\CompanyListResource;
use App\Http\Resources\Api\CompanyShowResource;
use App\Repositories\Api\CompanyRepository;
use App\Traits\ApiResponser;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CompanyController extends Controller
{
    use ApiResponser, ImageTrait;

    /**
     * CompanyController constructor.
     *
     * @param  CompanyRepository  $company
     */
    public function __construct(private CompanyRepository $company)
    {
        //
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->list) {

            $data = $this->company->getAll();

            if ($data) 
                return $this->success(CompanyListResource::collection($data),__('messages.retrieve',['X' => 'Company']));
        }

        if (request()->ajax()) { 

   
            $gets = $this->company->getTableData(request());

            return $this->responseJson($gets);

            // Log::info(print_r($gets,true));
        }

        return $this->error(__('messages.error'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCompanyRequest $request)
    {
        $params = $this->company->getNewParam($request);

        $data = $this->company->create($params);

        if($data) return $this->success($data, __('messages.retrieve',['X' => 'Company']));

        return $this->error(__('messages.error'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = $this->company->getShowData($id);

        if ($data) return $this->success(CompanyShowResource::make($data),trans('messages.retrieve',['X' => 'Company']));

        return $this->error(__('messages.retrieve_error'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCompanyRequest $request, string $id)
    {
        $data = $this->company->getByUuid($id);   
        
        if ($data) {

            $params = $this->company->getNewParam(
                $request,
                $data->logo
            );
            $data->update($params);
    
            return $this->success($data,trans('messages.update',['X' => 'Company']));
        }
        
        return $this->error(__('messages.retrieve_error'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = $this->company->getByUuid($id);   

        if ($data) {

            $this->removeImage($data->logo);
            $data->delete();

            return $this->success([],trans('messages.delete',['X' => 'Company']));
        }

        return $this->error(__('messages.retrieve_error'));
    }
}

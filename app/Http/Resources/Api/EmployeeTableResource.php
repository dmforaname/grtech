<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class EmployeeTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->uuid,
            'name' => Str::headline($this->full_name),
            'email' => $this->email,
            'phone' => $this->phone,
            'company' => [
                'id' => $this->company->uuid,
                'name' => $this->company->name,
                'website' => $this->company->website,
            ],
            'created_at' => $this->created_at
        ];
    }
}

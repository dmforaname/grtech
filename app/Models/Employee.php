<?php

namespace App\Models;

use App\Traits\BlameableTrait;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use BlameableTrait,UuidTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'company_id'
    ];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
